/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Button} from 'react-native';
import {
  createStackNavigator,
} from 'react-navigation';

import Login from './src/components/Login/Login';
import Register from './src/components/Register/Register';
import Welcome from './src/components/Welcome/Welcome';
import Main from './src/components/Main/Main';
import Verification from './src/components/Verification/Verification';
import RegisterForm2 from './src/components/Register/RegisterForm2';
import Dashboard from './src/components/Dashboard/Dashboard';
import BottomBar from './src/components/Bottombar/Bottombar';
import Drawer from './src/components/Drawer/Drawer';
import PersonalInfoForm from './src/components/Register/PersonalInfoForm';
import EducationForm from './src/components/Register/EducationForm';
import AppearanceAndWork from './src/components/Register/AppearanceAndWork';
import Questions from './src/components/Register/Questions';
import Polygamy from './src/components/Register/Polygamy';
import Chaperon from './src/components/Register/Chaperon';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});


const App = createStackNavigator({
  Main: { screen: Main },
  Dashboard: {screen: Dashboard,
    navigationOptions: {
      header: null,
    }
  },
  Welcome: { screen: Welcome },
  Login: { screen: Login },
  Register: { screen: Register },
  RegisterForm2: { screen: RegisterForm2 },
  Verification: {screen: Verification},
  BottomBar: {screen: BottomBar,
  },
  PersonalInfoForm: {screen: PersonalInfoForm},
  EducationForm: { screen: EducationForm },
  AppearanceAndWork: { screen: AppearanceAndWork },
  Questions: { screen: Questions },
  Polygamy: { screen: Polygamy },
  Chaperon: { screen: Chaperon },
  Drawer: {screen: Drawer,
    navigationOptions: {
      header: null,
    }
  },

},{
  headerMode: "screen",
}
);

export default class App1 extends Component {
  render() {
      return (
          <App/>
      );
  }
}
