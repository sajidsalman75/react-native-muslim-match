import React, {Component} from 'react';
import {StyleSheet, View, Image, ImageBackground, Text} from 'react-native';
import Toast from 'react-native-simple-toast';
import {
  createMaterialTopTabNavigator
} from 'react-navigation';

import Views from './Views';
import Messages from './Messages';
import Favourites from './Favourites';
import Blocked from './Blocked';
import PhotoRequests from './PhotoRequests';

class Dashboard extends Component {
    render() {
    return (
      <Messages/>
    );
  }
}

export default createMaterialTopTabNavigator({
  Messages: {screen: Messages},
  Views: {screen: Views},
  Favourites: {screen: Favourites},
  Blocked: {screen: Blocked},
  PhotoRequests: {screen: PhotoRequests},
},
{
  initialRouteName: 'Messages',
  tabBarOptions: {
    scrollEnabled: true,
    activeTintColor: '#114d91',
    style: {
      backgroundColor: '#cb1b7b',
      opacity: 1,
    },
    indicatorStyle: {
      backgroundColor: '#114d91'
    }
  }
});

