import React, { Component } from 'react';
import { NavigationActions, DrawerItems } from 'react-navigation';
import { ScrollView, Text, View, StyleSheet, Image } from 'react-native';
import PropTypes from 'prop-types';
class DrawerContent extends Component {
navigateToScreen = (route) => () => {
    const navigate = NavigationActions.navigate({
      routeName: route
    });
    this.props.navigation.dispatch(navigate);
  }
render () {
    return (
      <View style={styles.container}>
        <View style={styles.imageContainer}>
            <Image source={require('../../images/main.jpg')} style={styles.image} />
            <Text style={{color: '#ffffff'}}>Razia Sultan</Text>
        </View>
        <ScrollView style={{width: '100%', height: '100%'}}>
            <DrawerItems {...this.props}
            />
        </ScrollView>
        <View style={styles.bottomTextContainer}>
            <Text style={{color: '#ffffff'}}>30 days premiuim free</Text>
            <Text style={{color: '#ffffff'}}>for inviting a friend</Text>
        </View>
      </View>
    );
  }
}
DrawerContent.propTypes = {
  navigation: PropTypes.object
};

const styles = StyleSheet.create({
    imageContainer: {
        padding: 10,
        alignItems: "center",
        backgroundColor: "#cb1b7b",
    },
    bottomTextContainer: {
        padding: 10,
        backgroundColor: "#cb1b7b",
    },
    container: {
        width: '100%',
        height: '100%',
        backgroundColor: '#114d91',
    },
    textStyle: {
        color: '#ffffff',
        marginLeft: 10,
    },
    buttonContainer:{
        flex: 1,
        paddingLeft: 10,
        flexDirection: 'row',
        borderColor: '#cb1b7b',
        backgroundColor: "#cb1b7b",
    },
    activeButtonContainer:{
        flex: 1,
        paddingLeft: 10,
        flexDirection: 'row',
        borderColor: '#114d91',
        backgroundColor: "#114d91",
    },
    buttonText:{
        color: '#fff',
        marginLeft: 10,
    },
    drawerItemContainer: {
        flex: 1, 
        flexDirection: 'row',
        marginBottom: 20,
    },
    image: {
        width: 100,
        height: 100,
        borderRadius: 50,
        borderWidth: 2,
        borderColor: '#ffffff',
        marginBottom: 5,
      },
  });

export default DrawerContent;