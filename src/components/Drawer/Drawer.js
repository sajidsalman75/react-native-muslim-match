import React, {Component} from 'react';
import {Image, Button, Text,View, StyleSheet} from 'react-native';
import {DrawerNavigator, DrawerActions} from 'react-navigation';
import Bottombar from '../Bottombar/Bottombar';
import {Header, Left, Top, Right} from 'native-base';
import {Icon} from 'react-native-elements';
import DrawerContent from './SideBar';
import Premium from './PremiumAndExtras';
import ContactUs from './ContactUs';
import EditProfile from './EditProfile';
import Filters from './FiltersAndPreferences';
import Help from './HelpAndFAQ';
import Lock from './LockApp';
import Settings from './Settings';

export default class Drawer1 extends Component {
    constructor(props) {
        super(props);
      }
    render() {
        return (
            <Drawer>
                <Bottombar></Bottombar>
            </Drawer>
        );
    }
}
class Page1 extends Component{
    render() {
        const { navigate } = this.props.navigation;
        return (
            <Button title="Hello" onPress={() => {
                this.props.navigation.toggleDrawer();
            }} />
            
        );
      }
  }
  class Page2 extends Component{
    render() {
        return (
            <View>
                <Header>
        <Left>
          <Icon name="md-person" size={30} color="#4F8EF7"
          onPress={() => this.props.navigation.dispatch(DrawerActions.openDrawer())}
          />
        </Left>
      </Header>
      <Text>Hello</Text>
            </View>
            
        );
      }
  }
  class Page3 extends Component{
    render() {
        return (
            <Text>Hello2</Text>
        );
      }
  }
  const Drawer = DrawerNavigator({
    Home: {
      screen: Bottombar,
      navigationOptions: {
        drawerIcon: () => (<Icon name="home" color='#ffffff' />),
        drawerLabel: 'Home',
      }
    },
    Premium: {
      screen: Premium,
      navigationOptions: {
        drawerIcon: () => (<Icon name="update" color='#ffffff' />),
        drawerLabel: 'Premium & Extras',
      }
    },
    Filters: {
        screen: Filters,
        navigationOptions: {
          drawerIcon: () => (<Icon name="filter-list" color='#ffffff' />),
          drawerLabel: 'Filter & Preferences',
        }
      },
    EditProfile: {
      screen: EditProfile,
      navigationOptions: {
        drawerIcon: () => (<Icon name="edit" color='#ffffff' />),
        drawerLabel: 'Edit Profile',
      }
    },
    Settings: {
        screen: Settings,
        navigationOptions: {
          drawerIcon: () => (<Icon name="settings" color='#ffffff' />),
        }
    },
    Lock: {
        screen: Lock,
        navigationOptions: {
          drawerIcon: () => (<Icon name="lock" color='#ffffff' />),
          drawerLabel: 'Lock App',
        }
    },
    Help: {
        screen: Help,
        navigationOptions: {
          drawerIcon: () => (<Icon name="help" color='#ffffff' />),
          drawerLabel: 'Help FAQ',
        }
    },
    Contact: {
        screen: ContactUs,
        navigationOptions: {
          drawerIcon: () => (<Icon name="contacts" color='#ffffff' />),
          drawerLabel: 'Contact Us',
        }
    },
    Contact1: {
        screen: ContactUs,
        navigationOptions: {
          drawerIcon: () => (<Icon name="contacts" color='#ffffff' />),
          drawerLabel: 'Contact Us',
        }
    },
    Contact2: {
        screen: ContactUs,
        navigationOptions: {
          drawerIcon: () => (<Icon name="contacts" color='#ffffff' />),
          drawerLabel: 'Contact Us',
        }
    },
  }, {
    drawerWidth: 300,
    contentComponent: DrawerContent,
    contentOptions: {
        activeBackgroundColor: '#cb1b7b',
        activeTintColor: '#ffffff',
        inactiveTintColor: '#ffffff',
    }
  });