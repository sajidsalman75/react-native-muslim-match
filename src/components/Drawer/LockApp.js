import React, {Component} from 'react';
import {StyleSheet, View, Image, ImageBackground, TextInput, TouchableOpacity, Text} from 'react-native';
import Toast from 'react-native-simple-toast';


export default class Welcome extends Component {
    constructor(props) {
        super(props);
        const status = this.props.navigation.getParam('status', 'Error sending email!');
        Toast.show(status);
        Toast.show("Click on the verification button for Register form example!");
        this.state = { 
            codePlaceholder: 'Verification Code',
            verificationCode: '',
            verificationCodeToCheck: '123456',
        };
    }
    static navigationOptions = {
        headerTransparent: 'true',
        headerTintColor: 'white',
    };
    handleChange = (text) => {
        this.setState({verificationCode: text});
        if(text === this.state.verificationCodeToCheck){
            this.props.navigation.push('Register');
        }
    }
  
    render() {
    return (
    <ImageBackground source={require('../../images/bg.png')} style={{width: '100%', height: '100%'}} >
        <View style={styles.container}>
            <View style={styles.loginContainer}>
                <Image style={styles.logo}
                resizeMode="contain"
                source={require('../../images/logo.png')}
                />
            </View>
            <View style={styles.inputContainer}>
                <TextInput style = {styles.input}
                    autoCapitalize="none"
                    autoCorrect={false}
                    returnKeyType="next"
                    keyboardType= "numeric"
                    placeholder={this.state.codePlaceholder}
                    placeholderTextColor='#ffffff' 
                    onChangeText={this.handleChange}
                    value= {this.state.verificationCode}
                    />
            </View>
            <TouchableOpacity
            style={styles.buttonContainer}
            activeOpacity = { .9 }
            onPress={() => {
                this.props.navigation.push('Register');
            }}
            >
                <Text style={styles.buttonText}> Resend Code </Text>
            </TouchableOpacity>
        </View>
    </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
    },
    buttonContainer: {
      marginBottom: 10,
    },
    loginContainer:{
        alignItems: 'center',
        height: 200,
        justifyContent: 'center'
    },
    logo: {
        position: 'absolute',
        width: 300,
        height: 100
    },
    inputContainer: {
        marginBottom: 10,
        borderColor: "#ffffff", 
        borderBottomWidth: 1, 
        padding: 0, 
        margin: 0,
    },
    input:{
        color: '#fff',
        fontSize: 22,
    },
    buttonContainer:{
        borderRadius: 10,
        textAlign: 'center',
        fontWeight: '700',
        borderWidth: 1,
        borderColor: '#cb1b7b',
        paddingVertical: 10,
        marginTop: 10,
        backgroundColor: "#cb1b7b",
    },
    buttonText:{
        color: '#fff',
        textAlign: 'center',
        fontWeight: '700',
    },
});
