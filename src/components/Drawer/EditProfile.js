import React, {Component} from 'react';
import {StyleSheet, View, Image, ImageBackground, TextInput, TouchableOpacity, Text} from 'react-native';
import Toast from 'react-native-simple-toast';
import {
    createStackNavigator,
  } from 'react-navigation';

import Register from '../Register/Register';
import RegisterForm2 from '../Register/RegisterForm2';
import PersonalInfoForm from '../Register/PersonalInfoForm';
import EducationForm from '../Register/EducationForm';
import AppearanceAndWork from '../Register/AppearanceAndWork';
import Questions from '../Register/Questions';
import Polygamy from '../Register/Polygamy';
import Chaperon from '../Register/Chaperon';

const EditProfile = createStackNavigator({
    Register: { screen: Register },
    RegisterForm2: { screen: RegisterForm2 },
    PersonalInfoForm: {screen: PersonalInfoForm},
    EducationForm: { screen: EducationForm },
    AppearanceAndWork: { screen: AppearanceAndWork },
    Questions: { screen: Questions },
    Polygamy: { screen: Polygamy },
    Chaperon: { screen: Chaperon },
  },{
    headerMode: "screen",
  }
  );

export default class Welcome extends Component {
    constructor(props) {
        super(props);
    }
    static navigationOptions = {
        headerTransparent: 'true',
        headerTintColor: 'white',
    };
  
    render() {
    return (
        <EditProfile></EditProfile>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
    },
    buttonContainer: {
      marginBottom: 10,
    },
    loginContainer:{
        alignItems: 'center',
        height: 200,
        justifyContent: 'center'
    },
    logo: {
        position: 'absolute',
        width: 300,
        height: 100
    },
    inputContainer: {
        marginBottom: 10,
        borderColor: "#ffffff", 
        borderBottomWidth: 1, 
        padding: 0, 
        margin: 0,
    },
    input:{
        color: '#fff',
        fontSize: 22,
    },
    buttonContainer:{
        borderRadius: 10,
        textAlign: 'center',
        fontWeight: '700',
        borderWidth: 1,
        borderColor: '#cb1b7b',
        paddingVertical: 10,
        marginTop: 10,
        backgroundColor: "#cb1b7b",
    },
    buttonText:{
        color: '#fff',
        textAlign: 'center',
        fontWeight: '700',
    },
});
