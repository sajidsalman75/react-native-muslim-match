import React, {Component} from 'react';
import {StyleSheet, Text, View, KeyboardAvoidingView, Image, ImageBackground} from 'react-native';

import LoginForm from './LoginForm';

export default class Login extends Component {
  render() {
    return (
      <ImageBackground source={require('./bg.png')} style={{width: '100%', height: '100%'}} >
      <KeyboardAvoidingView behavior="padding" style={styles.container}>
      <View style={styles.container}>
        <View style={styles.loginContainer}>
          <Image style={styles.logo}
          resizeMode="contain"
          source={require('./logo.png')}
          />
        </View>
        <View style={styles.formContainer}>
          <LoginForm navigate={this.props.navigation} />
        </View>
      </View>
      </KeyboardAvoidingView>
    </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    loginContainer:{
        alignItems: 'center',
        marginTop: 40,
        height: 100,
        justifyContent: 'center'
    },
    logo: {
        position: 'absolute',
        width: 300,
        height: 100
    }
  });
