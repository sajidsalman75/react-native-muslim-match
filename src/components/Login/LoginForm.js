import React, {Component} from 'react';
import { View, Text, TextInput, TouchableOpacity,StyleSheet, Button} from 'react-native';

export default class LoginForm extends Component {
  render() {
    const { navigate } = this.props.navigate;
    return (
      <View style={styles.container}>
        <View style={styles.inputContainer}>
          <TextInput style = {styles.input1}
               autoCapitalize="none"
               onSubmitEditing={() => this.passwordInput.focus()}
               autoCorrect={false}
               returnKeyType="next"
               placeholder='Username/Email'
               placeholderTextColor='#ffffff' />
       </View>
       <View style={styles.inputContainer}>
         <TextInput style = {styles.input1}
              returnKeyType="go"
              ref={(input)=> this.passwordInput = input}
              placeholder='Password'
              placeholderTextColor='#ffffff'
              secureTextEntry />
       </View>
       <Text style={{color: '#fff',
       fontWeight: '700',}}>Remember Me</Text>
       <TouchableOpacity
          style={styles.buttonContainer}
          activeOpacity = { .9 }
       >
       <Text style={styles.buttonText}> Login </Text>
     </TouchableOpacity>
     <Text style={{textDecorationLine: "underline",color: '#fff',textAlign: "center", marginBottom: 10, marginTop: 10}}
     >Forgot Password</Text>

   <Text style={{color: '#fff',
   fontWeight: '700', textAlign: "center"}}>
         New User? <Text style={{color: "#cb1b7b"}}
         onPress={() =>
           navigate('Register')
         }
         >Sign Up!</Text>
       </Text>
    </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
     padding: 50
    },
    inputContainer: {
      marginBottom: 10,
      borderColor: "#cb1b7b", borderBottomWidth: 2, padding: 0, margin: 0
    },
    input1:{
      color: '#fff',
    },
    input:{
        height: 40,
        marginBottom: 10,
        padding: 10,
        color: '#fff',
        borderColor: '#cb1b7b',
        borderBottomWidth: 2,
    },
    buttonContainer:{
      borderRadius: 25,
      color: '#fff',
      textAlign: 'center',
      fontWeight: '700',
      borderWidth: 1,
      borderColor: '#cb1b7b',
      paddingVertical: 10,
      marginTop: 10,
      backgroundColor: "#cb1b7b",
    },
    buttonText:{
      color: '#fff',
      textAlign: 'center',
      fontWeight: '700',
    }
});
