import React, {Component} from 'react';
import { View, ScrollView, TextInput, Picker,StyleSheet, Button, ImageBackground, KeyboardAvoidingView} from 'react-native';
import {Textarea, ListItem, CheckBox, Text, Body} from 'native-base';

export default class Polygamy extends Component {
  static navigationOptions = {
    headerTransparent: 'true',
    headerTintColor: 'white',
  };
  constructor(props) {
    super(props);
    this.state = {
        polygamy: {
            acceptPolygamy: '',
            polygamyUnderstanding: '',
        },
        acceptPolygamy: '',
        polygamyUnderstanding: '',
    };
  }
  render() {
    return (
    <ImageBackground source={require('../../images/bg.png')} style={{width: '100%', height: '100%'}} >
        <Text style={styles.mainHeading}>
            Polygamy
        </Text>
        <ScrollView>
            <KeyboardAvoidingView behavior="padding">
                <View style={styles.container}>
                    <View style={styles.formContainer}>
                        <View style={styles.inputContainer}>
                            <Text style={styles.labels}>Would you accept polygamy?</Text>
                            <Picker
                            selectedValue={this.state.acceptPolygamy}
                            onValueChange={(itemValue, itemIndex) => this.setState({acceptPolygamy: itemValue})}
                            style={styles.input}>
                                <Picker.Item label="Yes" value="yes" />
                                <Picker.Item label="No" value="no" />
                            </Picker>
                        </View>
                        <View style={styles.inputContainer}>
                            <Text style={styles.labels}>What is your understanding of Polygamy in Islam?</Text>
                            <Textarea rowSpan={3} bordered placeholder="Enter you answer....." style={styles.picker} placeholderTextColor="#ffffff" />
                        </View>
                    </View>
                </View>
            </KeyboardAvoidingView>
            <View style={styles.buttonContainer}>
                <Button style={styles.buttonText}
                title="Next"
                color="#cb1b7b"
                onPress={() => {
                    this.props.navigation.push('Chaperon');
                }}
            />
            </View>
        </ScrollView>
    </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
    container: {
      paddingBottom: 20,
      paddingLeft: 20,
      paddingRight: 20,
    },
    buttonContainer:{
        marginBottom: 10,
    },
    buttonText:{
        color: '#fff',
        textAlign: 'center',
        fontWeight: '700'
    },
    labels: {
      color: '#ffffff',
    },
    mainHeading: {
      fontWeight: "bold",
      fontSize: 20,
      textAlign: 'center',
      color: "#ffffff",
      marginBottom: 10,
      marginTop: 15,
    },
    inputContainer: {
      marginBottom: 10,
      borderColor: "#ffffff", borderBottomWidth: 1, padding: 0, margin: 0
    },
    input:{
      color: '#fff',
    },
});
