import React, {Component} from 'react';
import { View, ScrollView, TextInput, Picker,StyleSheet, Button, ImageBackground, KeyboardAvoidingView} from 'react-native';
import {Textarea, ListItem, CheckBox, Text, Body, List} from 'native-base';

export default class Questions extends Component {
  static navigationOptions = {
    headerTransparent: 'true',
    headerTintColor: 'white',
  };
  constructor(props) {
    super(props);
    this.state = {
      questionsForm: {
        questions: [],
      },
      questions: [],
      question: '',
    };
  }
  render() {
    return (
    <ImageBackground source={require('../../images/bg.png')} style={{width: '100%', height: '100%'}} >
      <Text style={styles.mainHeading}>
        Questions
      </Text>
      <ScrollView>
        <KeyboardAvoidingView behavior="padding">
          <View style={styles.container}>
            <View style={styles.formContainer}>
                <View style={styles.inputContainer}>
                    <TextInput style = {styles.input}
                    autoCapitalize="none"
                    onSubmitEditing={() => this.passwordInput.focus()}
                    autoCorrect={false}
                    returnKeyType="next"
                    placeholder='Enter Your Question'
                    onChangeText={(question) => this.setState({question})}
                    value={this.state.question}
                    placeholderTextColor='#ffffff' />
                </View>
                <View style={styles.buttonContainer}>
                    <Button style={styles.buttonText}
                    title="Add Question"
                    color="#cb1b7b"
                    onPress={() => {
                        this.state.questions.push(this.state.question);
                        this.setState({questions: this.state.questions});
                    }}
                    />
                </View>
            </View>
          </View>
        </KeyboardAvoidingView>
        <List>
        {
            this.state.questions.map((l) => (
            <ListItem
                key={l}
                title={l}
            >
                <Text style={{color: '#ffffff'}}>{l}</Text>
            </ListItem>
            ))
        }
                
            </List>
        <View style={styles.buttonContainer}>
          <Button style={styles.buttonText}
            title="Next"
            color="#cb1b7b"
            onPress={() => {
              this.props.navigation.push('Polygamy');
            }}
          />
        </View>
      </ScrollView>
    </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
    container: {
      paddingBottom: 20,
      paddingLeft: 20,
      paddingRight: 20,
    },
    buttonContainer:{
        marginBottom: 10,
    },
    buttonText:{
        color: '#fff',
        textAlign: 'center',
        fontWeight: '700'
    },
    labels: {
      color: '#ffffff',
    },
    mainHeading: {
      fontWeight: "bold",
      fontSize: 20,
      textAlign: 'center',
      color: "#ffffff",
      marginBottom: 10,
      marginTop: 15,
    },
    inputContainer: {
      marginBottom: 10,
      borderColor: "#ffffff", borderBottomWidth: 1, padding: 0, margin: 0
    },
    input:{
      color: '#fff',
    },
});
