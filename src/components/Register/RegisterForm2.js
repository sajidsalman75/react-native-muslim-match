import React, {Component} from 'react';
import { View, ScrollView, TextInput, Picker,StyleSheet, Button, ImageBackground, KeyboardAvoidingView} from 'react-native';
import {Textarea, ListItem, CheckBox, Text, Body} from 'native-base';

export default class RegisterForm2 extends Component {
  static navigationOptions = {
    headerTransparent: 'true',
    headerTintColor: 'white',
  };
  constructor(props) {
    super(props);
    this.state = {
      form2: {
        religiousness: '',
        salah: '',
        sect: '',
        nikkahUnderstanding: '',
        wifeRights: '',
        wifeResponsibility: '',
        husbandRights: '',
        husbandResponsibility: '',
        beard: '',
        hijab: '',
        revert: '',
        halal: '',
      },
      religiousness: '',
      salah: '',
      sect: '',
      nikkahUnderstanding: '',
      wifeRights: '',
      wifeResponsibility: '',
      husbandRights: '',
      husbandResponsibility: '',
      beard: '',
      hijab: '',
      revert: '',
      halal: '',
    };
  }
  render() {
    return (
    <ImageBackground source={require('../../images/bg.png')} style={{width: '100%', height: '100%'}} >
      <Text style={styles.mainHeading}>
        Religion
      </Text>
      <ScrollView>
        <KeyboardAvoidingView behavior="padding">
          <View style={styles.container}>
            <View style={styles.formContainer}>
              <View style={styles.inputContainer}>
                <Picker
                  selectedValue={this.state.religiousness}
                  onValueChange={(itemValue, itemIndex) => this.setState({religiousness: itemValue})}
                  style={styles.input}>
                    <Picker.Item label="Select Religiousness" value="nothing" />
                    <Picker.Item label="Religious" value="religious" />
                    <Picker.Item label="Moderate" value="moderate" />
                </Picker>
              </View>
              <View style={styles.inputContainer}>
                <Picker
                  selectedValue={this.state.sect}
                  onValueChange={(itemValue, itemIndex) => this.setState({sect: itemValue})}
                  style={styles.input}>
                    <Picker.Item label="Select Sect" value="nothing" />
                    <Picker.Item label="Sunni" value="sunni" />
                    <Picker.Item label="Shia" value="shia" />
                </Picker>
              </View>
              <View style={styles.inputContainer}>
                <Picker
                  selectedValue={this.state.salah}
                  onValueChange={(itemValue, itemIndex) => this.setState({salah: itemValue})}
                  style={styles.input}>
                    <Picker.Item label="Select Salah" value="nothing" />
                    <Picker.Item label="Everyday" value="everyday" />
                    <Picker.Item label="Sometimes" value="sometimes" />
                </Picker>
              </View>
              <View style={styles.inputContainer}>
              <Text style={styles.labels}>I wear Hijab</Text>
                <Picker
                  selectedValue={this.state.hijab}
                  onValueChange={(itemValue, itemIndex) => this.setState({hijab: itemValue})}
                  style={styles.input}>
                    <Picker.Item label="Yes" value="yes" />
                    <Picker.Item label="No" value="no" />
                </Picker>
              </View>
              <View style={styles.inputContainer}>
                <Text style={styles.labels}>I keep beard</Text>
                <Picker
                  selectedValue={this.state.beard}
                  onValueChange={(itemValue, itemIndex) => this.setState({beard: itemValue})}
                  style={styles.input}>
                  <Picker.Item label="Yes" value="yes" />
                  <Picker.Item label="No" value="no" />
                </Picker>
              </View>
              <View style={styles.inputContainer}>
                <Text style={styles.labels}>I am Revert</Text>
                <Picker
                  selectedValue={this.state.revert}
                  onValueChange={(itemValue, itemIndex) => this.setState({revert: itemValue})}
                  style={styles.input}>
                  <Picker.Item label="Yes" value="yes" />
                  <Picker.Item label="No" value="no" />
                </Picker>
              </View>
              <View style={styles.inputContainer}>
                <Text style={styles.labels}>I keep halal</Text>
                <Picker
                  selectedValue={this.state.halal}
                  onValueChange={(itemValue, itemIndex) => this.setState({halal: itemValue})}
                  style={styles.input}>
                  <Picker.Item label="Yes" value="yes" />
                  <Picker.Item label="No" value="no" />
                </Picker>
              </View>
              <View style={styles.inputContainer}>
                <Text style={styles.labels}>What is your understanding of Fiqh of Nikkah & Marriage?</Text>
                <Textarea rowSpan={3} bordered placeholder="Enter you answer....." style={styles.picker} placeholderTextColor="#ffffff" />
              </View>
              <View style={styles.inputContainer}>
                <Text style={styles.labels}>What are the rights of a Wife?</Text>
                <Textarea rowSpan={3} bordered placeholder="Enter you answer....." style={styles.picker} placeholderTextColor="#ffffff" />
              </View>
              <View style={styles.inputContainer}>
                <Text style={styles.labels}>What are the responsibilities of a Wife?</Text>
                <Textarea rowSpan={3} bordered placeholder="Enter you answer....." style={styles.picker} placeholderTextColor="#ffffff" />
              </View>
              <View style={styles.inputContainer}>
                <Text style={styles.labels}>What are the rights of a Husband?</Text>
                <Textarea rowSpan={3} bordered placeholder="Enter you answer....." style={styles.picker} placeholderTextColor="#ffffff" />
              </View>
              <View style={styles.inputContainer}>
                <Text style={styles.labels}>What are the responsibilities of a Husband?</Text>
                <Textarea rowSpan={3} bordered placeholder="Enter you answer....." style={styles.picker} placeholderTextColor="#ffffff" />
              </View>
            </View>
          </View>
        </KeyboardAvoidingView>
        <View style={styles.buttonContainer}>
          <Button style={styles.buttonText}
            title="Next"
            color="#cb1b7b"
            onPress={() => {
              this.props.navigation.push('PersonalInfoForm');
            }}
          />
        </View>
      </ScrollView>
    </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
    container: {
      paddingBottom: 20,
      paddingLeft: 20,
      paddingRight: 20,
    },
    buttonContainer:{
        marginBottom: 10,
    },
    buttonText:{
        color: '#fff',
        textAlign: 'center',
        fontWeight: '700'
    },
    labels: {
      color: '#ffffff',
    },
    mainHeading: {
      fontWeight: "bold",
      fontSize: 20,
      textAlign: 'center',
      color: "#ffffff",
      marginBottom: 10,
      marginTop: 15,
    },
    inputContainer: {
      marginBottom: 10,
      borderColor: "#ffffff", borderBottomWidth: 1, padding: 0, margin: 0
    },
    input:{
      color: '#fff',
    },
});
