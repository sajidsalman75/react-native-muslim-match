import React, {Component} from 'react';
import { View, ScrollView, TextInput, Picker,StyleSheet, Button, ImageBackground, KeyboardAvoidingView} from 'react-native';
import {Textarea, ListItem, CheckBox, Text, Body} from 'native-base';

export default class PersonalInfoForm extends Component {
  static navigationOptions = {
    headerTransparent: 'true',
    headerTintColor: 'white',
  };
  constructor(props) {
    super(props);
    this.state = {
        form2: {
            countryOfOrigin: '',
            citizenship: '',
            relocate: '',
            maritalStatus: '',
            children: '',
            noOfChildren: '',
            moreChildren: '',
            livingArrangements: '',
        },
        countryOfOrigin: '',  
        citizenship: '',
        relocate: '',
        maritalStatus: '',
        children: '',
        noOfChildren: '',
        moreChildren: '',
        livingArrangements: '',
    };
  }
  render() {
    return (
    <ImageBackground source={require('../../images/bg.png')} style={{width: '100%', height: '100%'}} >
        <Text style={styles.mainHeading}>
            Personal
        </Text>
        <ScrollView>
            <KeyboardAvoidingView behavior="padding">
                <View style={styles.container}>
                    <View style={styles.formContainer}>
                        <View style={styles.inputContainer}>
                            <Text style={styles.labels}>Country Of Origin</Text>
                            <Picker
                            selectedValue={this.state.countryOfOrigin}
                            onValueChange={(itemValue, itemIndex) => this.setState({countryOfOrigin: itemValue})}
                            style={styles.input}>
                                <Picker.Item label="Select Country" value="nothing" />
                                <Picker.Item label="Pakistan" value="pakistan" />
                                <Picker.Item label="India" value="india" />
                            </Picker>
                        </View>
                        <View style={styles.inputContainer}>
                            <Text style={styles.labels}>CitizenShip</Text>
                            <Picker
                            selectedValue={this.state.citizenship}
                            onValueChange={(itemValue, itemIndex) => this.setState({citizenship: itemValue})}
                            style={styles.input}>
                                <Picker.Item label="Select CitizenShip" value="nothing" />
                                <Picker.Item label="Pakistan" value="pakistan" />
                                <Picker.Item label="India" value="india" />
                            </Picker>
                        </View>
                        <View style={styles.inputContainer}>
                            <Text style={styles.labels}>Would you relocate?</Text>
                            <Picker
                            selectedValue={this.state.relocate}
                            onValueChange={(itemValue, itemIndex) => this.setState({relocate: itemValue})}
                            style={styles.input}>
                                <Picker.Item label="Yes" value="yes" />
                                <Picker.Item label="No" value="no" />
                            </Picker>
                        </View>
                        <View style={styles.inputContainer}>
                            <Text style={styles.labels}>Marital Status</Text>
                            <Picker
                            selectedValue={this.state.maritalStatus}
                            onValueChange={(itemValue, itemIndex) => this.setState({maritalStatus: itemValue})}
                            style={styles.input}>
                                <Picker.Item label="Married" value="married" />
                                <Picker.Item label="Single" value="single" />
                                <Picker.Item label="Divorced" value="divorced" />
                                <Picker.Item label="Window" value="widow" />
                            </Picker>
                        </View>
                        <View style={styles.inputContainer}>
                            <Text style={styles.labels}>Do you have children?</Text>
                            <Picker
                            selectedValue={this.state.children}
                            onValueChange={(itemValue, itemIndex) => this.setState({children: itemValue})}
                            style={styles.input}>
                            <Picker.Item label="Yes" value="yes" />
                            <Picker.Item label="No" value="no" />
                            </Picker>
                        </View>
                        <View style={styles.inputContainer}>
                            <Text style={styles.labels}>How many children do you have?</Text>
                            <TextInput style = {styles.input}
                            autoCapitalize="none"
                            onSubmitEditing={() => this.passwordInput.focus()}
                            autoCorrect={false}
                            returnKeyType="next"
                            placeholder='Number of Children'
                            onChangeText={(noOfChildren) => this.setState({noOfChildren})}
                            value={this.state.address}
                            placeholderTextColor='#ffffff' />
                        </View>
                        <View style={styles.inputContainer}>
                            <Text style={styles.labels}>Would you like more children?</Text>
                            <Picker
                            selectedValue={this.state.moreChildren}
                            onValueChange={(itemValue, itemIndex) => this.setState({moreChildren: itemValue})}
                            style={styles.input}>
                                <Picker.Item label="Yes" value="yes" />
                                <Picker.Item label="No" value="no" />
                            </Picker>
                        </View>
                        <View style={styles.inputContainer}>
                            <Text style={styles.labels}>Living Arrangements</Text>
                            <Picker
                            selectedValue={this.state.livingArrangements}
                            onValueChange={(itemValue, itemIndex) => this.setState({livingArrangements: itemValue})}
                            style={styles.input}>
                                <Picker.Item label="Own a house" value="own" />
                                <Picker.Item label="Rented" value="rented" />
                            </Picker>
                        </View>
                    </View>
                </View>
            </KeyboardAvoidingView>
            <View style={styles.buttonContainer}>
                <Button style={styles.buttonText}
                title="Next"
                color="#cb1b7b"
                onPress={() => {
                    this.props.navigation.push('EducationForm');
                }}
                />
            </View>
        </ScrollView>
    </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
    container: {
      paddingBottom: 20,
      paddingLeft: 20,
      paddingRight: 20,
    },
    buttonContainer:{
        marginBottom: 10
    },
    buttonText:{
        color: '#fff',
        textAlign: 'center',
        fontWeight: '700'
    },
    labels: {
      color: '#ffffff',
    },
    mainHeading: {
      fontWeight: "bold",
      fontSize: 20,
      textAlign: 'center',
      color: "#ffffff",
      marginBottom: 10,
      marginTop: 15,
    },
    inputContainer: {
      marginBottom: 10,
      borderColor: "#ffffff", borderBottomWidth: 1, padding: 0, margin: 0
    },
    input:{
      color: '#fff',
    },
});
