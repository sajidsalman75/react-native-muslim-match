import React, {Component} from 'react';
import { View, ScrollView, TextInput, Picker,StyleSheet, Button, ImageBackground, KeyboardAvoidingView} from 'react-native';
import {Textarea, ListItem, CheckBox, Text, Body} from 'native-base';

export default class AppearanceAndWork extends Component {
  static navigationOptions = {
    headerTransparent: 'true',
    headerTintColor: 'white',
  };
  constructor(props) {
    super(props);
    this.state = {
        appearance: {
            height: '',
            build: '',
            hairColor: '',
            eyesColor: '',
            disabilities: '',
            habits: '',
            profession: '',
            jobTitle: '',
            salary: '',
            languages: [],
        },
        height: '',
        build: '',
        hairColor: '',
        eyesColor: '',
        disabilities: '',
        habits: '',
        profession: '',
        jobTitle: '',
        salary: '',
        languages: [],
        language: '',
    };
  }
  render() {
    return (
    <ImageBackground source={require('../../images/bg.png')} style={{width: '100%', height: '100%'}} >
        <Text style={styles.mainHeading}>
            Appearance
        </Text>
        <ScrollView>
            <KeyboardAvoidingView behavior="padding">
                <View style={styles.container}>
                    <View style={styles.formContainer}>
                        <View style={styles.inputContainer}>
                            <Picker
                            selectedValue={this.state.height}
                            onValueChange={(itemValue, itemIndex) => this.setState({height: itemValue})}
                            style={styles.input}>
                                <Picker.Item label="Select Height" value="nothing" />
                                <Picker.Item label="5 ft 10 in" value="5 ft 10 in" />
                            </Picker>
                        </View>
                        <View style={styles.inputContainer}>
                            <Picker
                            selectedValue={this.state.build}
                            onValueChange={(itemValue, itemIndex) => this.setState({build: itemValue})}
                            style={styles.input}>
                                <Picker.Item label="Select Build" value="nothing" />
                                <Picker.Item label="Pakistan" value="pakistan" />
                                <Picker.Item label="India" value="india" />
                            </Picker>
                        </View>
                        <View style={styles.inputContainer}>
                            <Picker
                            selectedValue={this.state.hairColor}
                            onValueChange={(itemValue, itemIndex) => this.setState({hairColor: itemValue})}
                            style={styles.input}>
                                <Picker.Item label="Select Hair Color" value="nothing" />
                                <Picker.Item label="Black" value="black" />
                                <Picker.Item label="Blue" value="blue" />
                            </Picker>
                        </View>
                        <View style={styles.inputContainer}>
                            <Picker
                            selectedValue={this.state.eyesColor}
                            onValueChange={(itemValue, itemIndex) => this.setState({eyesColor: itemValue})}
                            style={styles.input}>
                                <Picker.Item label="Select Eyes Color" value="nothing" />
                                <Picker.Item label="Black" value="black" />
                                <Picker.Item label="Blue" value="blue" />
                                <Picker.Item label="White" value="white" />
                            </Picker>
                        </View>
                        <View style={styles.inputContainer}>
                            <TextInput style = {styles.input}
                            autoCapitalize="none"
                            onSubmitEditing={() => this.passwordInput.focus()}
                            autoCorrect={false}
                            returnKeyType="next"
                            placeholder='Enter your disablity'
                            onChangeText={(disablity) => this.setState({disabilities})}
                            value={this.state.address}
                            placeholderTextColor='#ffffff' />
                        </View>
                        <Text style={styles.mainHeading}>
                            Work
                        </Text>
                        <View style={styles.inputContainer}>
                            <TextInput style = {styles.input}
                            autoCapitalize="none"
                            onSubmitEditing={() => this.passwordInput.focus()}
                            autoCorrect={false}
                            returnKeyType="next"
                            placeholder='Profession'
                            onChangeText={(profession) => this.setState({profession})}
                            value={this.state.profession}
                            placeholderTextColor='#ffffff' />
                        </View>
                        <View style={styles.inputContainer}>
                            <TextInput style = {styles.input}
                            autoCapitalize="none"
                            onSubmitEditing={() => this.passwordInput.focus()}
                            autoCorrect={false}
                            returnKeyType="next"
                            placeholder='Job Title'
                            onChangeText={(jobTitle) => this.setState({jobTitle})}
                            value={this.state.jobTitle}
                            placeholderTextColor='#ffffff' />
                        </View>
                        <View style={styles.inputContainer}>
                            <TextInput style = {styles.input}
                            autoCapitalize="none"
                            onSubmitEditing={() => this.passwordInput.focus()}
                            autoCorrect={false}
                            returnKeyType="next"
                            placeholder='Salary'
                            onChangeText={(salary) => this.setState({salary})}
                            value={this.state.salary}
                            placeholderTextColor='#ffffff' />
                        </View>
                        <Text style={styles.mainHeading}>
                            Languages
                        </Text>
                        <View style={styles.inputContainer}>
                            <Text style={styles.labels}>What languages do you speak?</Text>
                            <Picker
                            selectedValue={this.state.language}
                            onValueChange={(itemValue, itemIndex) => this.setState({language: itemValue})}
                            style={styles.input}>
                                <Picker.Item label="Urdu" value="urdu" />
                                <Picker.Item label="English" value="english" />
                            </Picker>
                        </View>
                    </View>
                </View>
            </KeyboardAvoidingView>
            <View style={styles.buttonContainer}>
                <Button style={styles.buttonText}
                title="Next"
                color="#cb1b7b"
                onPress={() => {
                    this.props.navigation.push('Questions');
                }}
                />
            </View>
        </ScrollView>
    </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
    container: {
      paddingBottom: 20,
      paddingLeft: 20,
      paddingRight: 20,
    },
    buttonContainer:{
        marginBottom: 10
    },
    buttonText:{
        color: '#fff',
        textAlign: 'center',
        fontWeight: '700'
    },
    labels: {
      color: '#ffffff',
    },
    mainHeading: {
      fontWeight: "bold",
      fontSize: 20,
      textAlign: 'center',
      color: "#ffffff",
      marginBottom: 10,
      marginTop: 15,
    },
    inputContainer: {
      marginBottom: 10,
      borderColor: "#ffffff", borderBottomWidth: 1, padding: 0, margin: 0
    },
    input:{
      color: '#fff',
    },
});
