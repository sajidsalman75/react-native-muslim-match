import React, {Component} from 'react';
import { View, Text, TextInput, KeyboardAvoidingView,StyleSheet, Picker, Button, ScrollView} from 'react-native';
import DatePicker from 'react-native-datepicker';
import {Textarea} from 'native-base';

export default class RegisterForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      form1: {
        gender: undefined,
        state: '',
        date:"",
        country: "",
        name: "",
        city: '',
        status: "",
        address: '',
      },
      country: '',
      state: '',
      gender: undefined,
      date:"",
      name: "",
      road: "",
      city: '',
      address: '',
    };
  }

  handleSubmit = () => {
    this.state.form1.gender = this.state.gender;
    this.state.form1.state = this.state.state;
    this.state.form1.country = this.state.country;
    this.state.form1.city = this.state.city;
    this.state.form1.address = this.state.address;
    this.state.form1.date = this.state.date;
    this.state.form1.road = this.state.road;

    this.state.form1.status = "success";
    this.props.onSubmit1(this.state.form1);
  }

  render() {
    const { navigate } = this.props.navigate;
    return (
      <ScrollView>
        <KeyboardAvoidingView behavior="padding">
          <View style={styles.container}>
            <View style={styles.pickerContainer}>
              <TextInput style = {styles.picker}
                autoCapitalize="none"
                onSubmitEditing={() => this.passwordInput.focus()}
                autoCorrect={false}
                returnKeyType="next"
                placeholder='Name'
                onChangeText={(name) => this.setState({name})}
                value={this.state.name}
                placeholderTextColor='#ffffff' />
            </View>
            <View style={styles.pickerContainer}>
              <Picker
                  selectedValue={this.state.gender}
                  onValueChange={(itemValue, itemIndex) => this.setState({gender: itemValue})}
                  style={styles.picker}>
                  <Picker.Item label="Select Gender" value="nothing" />
                  <Picker.Item label="Male" value="male" />
                  <Picker.Item label="Female" value="female" />
              </Picker>
            </View>
            <View style={styles.pickerContainer}>
              <DatePicker
                style={{height: 40,
                marginBottom: 10,
                width: 340,
                color: '#fff'}}
                date={this.state.date}
                mode="date"
                placeholder="Date of Birth"
                format="YYYY-MM-DD"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                customStyles={{
                  dateIcon: {
                    position: 'absolute',
                    left: 0,
                    marginLeft: 10
                  },
                  dateInput: {
                    borderWidth: 0,
                  },
                  dateText: {
                    padding:0,
                    margin:0,
                    color: "#ffffff",
                    borderWidth:0,
                  }
                  // ... You can check the source to find the other keys.
                }}
                onDateChange={(date) => {this.setState({date: date})}}
                value={this.state.date}
              />
            </View>
            <View style={styles.pickerContainer}>
              <TextInput style = {styles.picker}
                autoCapitalize="none"
                onSubmitEditing={() => this.passwordInput.focus()}
                autoCorrect={false}
                returnKeyType="next"
                placeholder='Address'
                onChangeText={(address) => this.setState({address})}
                value={this.state.address}
                placeholderTextColor='#ffffff' />
            </View>
            <View style={styles.pickerContainer}>
              <TextInput style = {styles.picker}
                autoCapitalize="none"
                onSubmitEditing={() => this.passwordInput.focus()}
                autoCorrect={false}
                returnKeyType="next"
                placeholder='Road Name'
                onChangeText={(road) => this.setState({road})}
                value={this.state.road}
                placeholderTextColor='#ffffff' />
            </View>
            <View style={styles.pickerContainer}>
              <Picker
                  selectedValue={this.state.city}
                  onValueChange={(itemValue, itemIndex) => this.setState({city: itemValue})}
                  style={styles.picker}>
                  <Picker.Item label="City" value="nothing" />
                  <Picker.Item label="Lahore" value="lahore" />
                  <Picker.Item label="Islamabad" value="islamabad" />
                  <Picker.Item label="Karachi" value="karachi" />
              </Picker>
            </View>
            <View style={styles.pickerContainer}>
              <Picker
                  selectedValue={this.state.state}
                  onValueChange={(itemValue, itemIndex) => this.setState({state: itemValue})}
                  style={styles.picker}>
                  <Picker.Item label="State" value="nothing" />
                  <Picker.Item label="Punjab" value="punjab" />
                  <Picker.Item label="Balochistan" value="balochistan" />
                  <Picker.Item label="Sindh" value="sindh" />
              </Picker>
            </View>
            <View style={styles.pickerContainer}>
              <Picker
                  selectedValue={this.state.country}
                  onValueChange={(itemValue, itemIndex) => this.setState({country: itemValue})}
                  style={styles.picker}>
                  <Picker.Item label="Country" value="nothing" />
                  <Picker.Item label="Pakistan" value="pakistan" />
                  <Picker.Item label="India" value="india" />
                  <Picker.Item label="Turkey" value="turkey" />
              </Picker>
            </View>
            <Textarea rowSpan={3} bordered placeholder="Reason to join" style={styles.picker} placeholderTextColor="#ffffff" />
          </View>
          <Button style={styles.buttonText}
            title="Next"
            color="#cb1b7b"
            onPress={this.handleSubmit}
            />
          <Text style={styles.bottomText}>
            Have an account? <Text style={{textDecorationLine: "underline"}}
            onPress={() =>
              navigate('Login')
            }
            >Sign in!</Text>
          </Text>
        </KeyboardAvoidingView>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
    container: {
     paddingBottom: 20,
     paddingLeft: 20,
     paddingRight: 20,
    },
    pickerContainer: {
      marginBottom: 10,
      borderColor: "#ffffff", borderBottomWidth: 1, padding: 0, margin: 0
    },
    picker:{
      color: '#fff',
    },
    buttonContainer:{
        backgroundColor: '#cb1b7b',
        paddingVertical: 15
    },
    buttonText:{
        color: '#fff',
        textAlign: 'center',
        fontWeight: '700'
    },
    mainHeading: {
      fontWeight: "bold",
      fontSize: 20,
      textAlign: 'center',
      color: "#ffffff"
    },
    bottomText:{
      textAlign: "center",
      color: "#ffffff",
      marginTop: 10,
    }
});
