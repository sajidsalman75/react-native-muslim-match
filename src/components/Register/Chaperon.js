import React, {Component} from 'react';
import { View, ScrollView, TextInput, Picker,StyleSheet, Button, ImageBackground, KeyboardAvoidingView} from 'react-native';
import {Textarea, ListItem, CheckBox, Text, Body} from 'native-base';

export default class RegisterForm2 extends Component {
    static navigationOptions = {
        headerTransparent: 'true',
        headerTintColor: 'white',
    };
    constructor(props) {
        super(props);
        this.state = {
            chaperon: {
                name: '',
                email: '',
                contact: '',
                relationship: '',
            },
            name: '',
            email: '',
            contact: '',
            relationship: '',
        };
    }
    render() {
        return (
        <ImageBackground source={require('../../images/bg.png')} style={{width: '100%', height: '100%'}} >
            <Text style={styles.mainHeading}>
                Chaperon/Wali
            </Text>
            <ScrollView>
                <KeyboardAvoidingView behavior="padding">
                    <View style={styles.container}>
                        <View style={styles.formContainer}>
                            <View style={styles.inputContainer}>
                                <TextInput style = {styles.input}
                                autoCapitalize="none"
                                onSubmitEditing={() => this.passwordInput.focus()}
                                autoCorrect={false}
                                returnKeyType="next"
                                placeholder='Enter Your Name'
                                onChangeText={(name) => this.setState({name})}
                                value={this.state.name}
                                placeholderTextColor='#ffffff' />
                            </View>
                            <View style={styles.inputContainer}>
                                <TextInput style = {styles.input}
                                autoCapitalize="none"
                                onSubmitEditing={() => this.passwordInput.focus()}
                                autoCorrect={false}
                                returnKeyType="next"
                                placeholder='Enter Your Email Address'
                                onChangeText={(email) => this.setState({email})}
                                value={this.state.email}
                                placeholderTextColor='#ffffff' />
                            </View>
                            <View style={styles.inputContainer}>
                                <TextInput style = {styles.input}
                                autoCapitalize="none"
                                onSubmitEditing={() => this.passwordInput.focus()}
                                autoCorrect={false}
                                returnKeyType="next"
                                placeholder='Enter Your Phone Number'
                                onChangeText={(contact) => this.setState({contact})}
                                value={this.state.contact}
                                placeholderTextColor='#ffffff' />
                            </View>
                            <View style={styles.inputContainer}>
                                <Picker
                                selectedValue={this.state.relationship}
                                onValueChange={(itemValue, itemIndex) => this.setState({relationship: itemValue})}
                                style={styles.input}>
                                    <Picker.Item label="Select Relationship" value="nothing" />
                                    <Picker.Item label="Uncle" value="uncle" />
                                    <Picker.Item label="Father" value="father" />
                                </Picker>
                            </View>
                        </View>
                    </View>
                </KeyboardAvoidingView>
                <View style={styles.buttonContainer}>
                <Button style={styles.buttonText}
                    title="Next"
                    color="#cb1b7b"
                    onPress={() => {
                    this.props.navigation.push('Drawer');
                    }}
                />
                </View>
            </ScrollView>
        </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
    container: {
      paddingBottom: 20,
      paddingLeft: 20,
      paddingRight: 20,
    },
    buttonContainer:{
        marginBottom: 10,
    },
    buttonText:{
        color: '#fff',
        textAlign: 'center',
        fontWeight: '700'
    },
    labels: {
      color: '#ffffff',
    },
    mainHeading: {
      fontWeight: "bold",
      fontSize: 20,
      textAlign: 'center',
      color: "#ffffff",
      marginBottom: 10,
      marginTop: 15,
    },
    inputContainer: {
      marginBottom: 10,
      borderColor: "#ffffff", borderBottomWidth: 1, padding: 0, margin: 0
    },
    input:{
      color: '#fff',
    },
});
