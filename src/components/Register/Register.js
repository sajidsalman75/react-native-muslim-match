import React, {Component} from 'react';
import {StyleSheet, Text, View, KeyboardAvoidingView, Image, ImageBackground} from 'react-native';
import Toast from 'react-native-simple-toast';

import RegisterForm from './RegisterForm';

export default class Register extends Component {
  constructor(props) {
    super(props);
    Toast.show("Click on the Next button for Bottom Navigation bar example!");
    this.state = {
      registerForm1: {
        name: '',
        createdFor: '',
        religion: '',
        gender: '',
        dateOfBirth: '',
        motherTongue: '',
        countryCode: '',
        status: '',
      }
    };
  }
  static navigationOptions = {
    headerTransparent: 'true',
    headerTintColor: 'white',
  };
  handleSubmit1 = (form1) => {
    this.setState({registerForm1: form1});
    this.props.navigation.push('RegisterForm2');
  }
  render() {
    const { navigate } = this.props.navigation;
    return (
      <ImageBackground source={require('../../images/bg.png')} style={{width: '100%', height: '100%'}} >
      <KeyboardAvoidingView behavior="padding" style={styles.container}>
        <Text style={styles.mainHeading}>
          Registertion
        </Text>
      <View style={styles.container}>
        <View style={styles.formContainer}>
          <RegisterForm navigate={this.props.navigation}  onSubmit1={this.handleSubmit1} />
        </View>
      </View>
      </KeyboardAvoidingView>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    loginContainer:{
      flexGrow: 1,
      alignItems: 'center',
      justifyContent: 'center'
    },
    logo: {
        position: 'absolute',
        width: 300,
        height: 100
    },
    mainHeading: {
      fontWeight: "bold",
      fontSize: 20,
      textAlign: 'center',
      color: "#ffffff",
      marginBottom: 10,
      marginTop: 15,
    },
  });
