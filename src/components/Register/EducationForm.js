import React, {Component} from 'react';
import { View, ScrollView, TextInput, Picker,StyleSheet, Button, ImageBackground, KeyboardAvoidingView} from 'react-native';
import {Textarea, ListItem, CheckBox, Text, Body} from 'native-base';

export default class PersonalInfoForm extends Component {
  static navigationOptions = {
    headerTransparent: 'true',
    headerTintColor: 'white',
  };
  constructor(props) {
    super(props);
    this.state = {
        education: {
            islamicEducationLevel: '',
            islamicSchoolAttended: '',
            professionalEducationLevel: '',
            professionalCourseStudied: '',
        },
        islamicEducationLevel: '',
        islamicSchoolAttended: '',
        professionalEducationLevel: '',
        professionalCourseStudied: '',
    };
  }
  render() {
    return (
    <ImageBackground source={require('../../images/bg.png')} style={{width: '100%', height: '100%'}} >
        <Text style={styles.mainHeading}>
            Education
        </Text>
        <ScrollView>
            <KeyboardAvoidingView behavior="padding">
                <View style={styles.container}>
                    <View style={styles.formContainer}>
                        <View style={styles.inputContainer}>
                            <Picker
                            selectedValue={this.state.islamicEducationLevel}
                            onValueChange={(itemValue, itemIndex) => this.setState({islamicEducationLevel: itemValue})}
                            style={styles.input}>
                                <Picker.Item label="Select Islamic Education Level" value="nothing" />
                                <Picker.Item label="Degree" value="degree" />
                            </Picker>
                        </View>
                        <View style={styles.inputContainer}>
                            <Picker
                            selectedValue={this.state.islamicSchoolAttended}
                            onValueChange={(itemValue, itemIndex) => this.setState({islamicSchoolAttended: itemValue})}
                            style={styles.input}>
                                <Picker.Item label="Select Islamic School Attended" value="nothing" />
                                <Picker.Item label="Dummy" value="dummy" />
                            </Picker>
                        </View>
                        <View style={styles.inputContainer}>
                            <Picker
                            selectedValue={this.state.professionalEducationLevel}
                            onValueChange={(itemValue, itemIndex) => this.setState({professionalEducationLevel: itemValue})}
                            style={styles.input}>
                                <Picker.Item label="Select Professional Education Level" value="nothing" />
                                <Picker.Item label="Degree" value="degree" />
                            </Picker>
                        </View>
                        <View style={styles.inputContainer}>
                            <Picker
                            selectedValue={this.state.professionalCourseStudied}
                            onValueChange={(itemValue, itemIndex) => this.setState({professionalCourseStudied: itemValue})}
                            style={styles.input}>
                                <Picker.Item label="Select Professional Course Studied" value="nothing" />
                                <Picker.Item label="BS Computer Science" value="cs" />
                            </Picker>
                        </View>
                    </View>
                </View>
            </KeyboardAvoidingView>
            <View style={styles.buttonContainer}>
                <Button style={styles.buttonText}
                title="Next"
                color="#cb1b7b"
                onPress={() => {
                    this.props.navigation.push('AppearanceAndWork');
                }}
                />
            </View>
        </ScrollView>
    </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
    container: {
      paddingBottom: 20,
      paddingLeft: 20,
      paddingRight: 20,
    },
    buttonContainer:{
        marginBottom: 10,
    },
    buttonText:{
        color: '#fff',
        textAlign: 'center',
        fontWeight: '700'
    },
    labels: {
      color: '#ffffff',
    },
    mainHeading: {
      fontWeight: "bold",
      fontSize: 20,
      textAlign: 'center',
      color: "#ffffff",
      marginBottom: 10,
      marginTop: 15,
    },
    inputContainer: {
      marginBottom: 10,
      borderColor: "#ffffff", borderBottomWidth: 1, padding: 0, margin: 0
    },
    input:{
      color: '#fff',
    },
});
