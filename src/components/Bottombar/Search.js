import React, {Component} from 'react';
import { View, Text, TextInput, TouchableOpacity,StyleSheet, Button, ImageBackground, KeyboardAvoidingView} from 'react-native';

export default class Favourites extends Component {
  constructor(props) {
    super(props);
    this.state = {
      form2: {
        
      }
    };
  }
  render() {
    return (
    <ImageBackground source={require('../../images/bg.png')} style={{width: '100%', height: '100%'}} >
      <KeyboardAvoidingView behavior="padding">
        <Text style={styles.mainHeading}>
          Registertion
        </Text>
        <View style={styles.container}>
          <View style={styles.formContainer}>
            <View style={styles.inputContainer}>
              <TextInput style = {styles.input}
                autoCapitalize="none"
                autoCorrect={false}
                onChangeText={(email) => this.setState({email})}
                keyboardType='email-address'
                returnKeyType="next"
                placeholder='Email'
                placeholderTextColor='#ffffff' />
            </View>
            <View style={styles.inputContainer}>
              <TextInput style = {styles.input}
                autoCapitalize="none"
                autoCorrect={false}
                onChangeText={(password) => this.setState({password})}
                returnKeyType="next"
                placeholder='Password'
                placeholderTextColor='#ffffff'
                secureTextEntry />
            </View>
            <View style={styles.inputContainer}>
              <TextInput style = {styles.input}
                autoCapitalize="none"
                autoCorrect={false}
                onChangeText={(password) => this.setState({password})}
                returnKeyType="go"
                placeholder='Location'
                placeholderTextColor='#ffffff' />
            </View>
          </View>
        </View>
      </KeyboardAvoidingView>
    </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
    container: {
      paddingBottom: 20,
      paddingLeft: 20,
      paddingRight: 20,
    },
    buttonContainer:{
        backgroundColor: '#cb1b7b',
        paddingVertical: 15
    },
    buttonText:{
        color: '#fff',
        textAlign: 'center',
        fontWeight: '700'
    },
    mainHeading: {
      fontWeight: "bold",
      fontSize: 20,
      textAlign: 'center',
      color: "#ffffff",
      marginBottom: 10,
      marginTop: 15,
    },
    inputContainer: {
      marginBottom: 10,
      borderColor: "#ffffff", borderBottomWidth: 1, padding: 0, margin: 0
    },
    input:{
      color: '#fff',
    },
});
