import React, {Component} from 'react';
import {Button} from 'react-native';

export default class MenuButton extends Component {
    constructor(props) {
        super(props); 
    }

    openDrawer = () => {
        this.props.navigation.toggleDrawer();
    }
  render() {
    return (
      <Button
      title="Menu"
      onPress={this.openDrawer}
      ></Button>
    );
  }
}