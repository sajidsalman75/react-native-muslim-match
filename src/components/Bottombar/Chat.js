import React, {Component} from 'react';
import { View, Text, TextInput,StyleSheet,Image, ImageBackground, KeyboardAvoidingView, ScrollView, FlatList} from 'react-native';

export default class RegisterForm2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      form2: {
        
      }
    };
  }
  render() {
    return (
      <ScrollView contentContainerStyle={styles.contentContainer}>
        <ImageBackground source={require('../../images/bg.png')} style={{width: '100%', height: '100%'}} >
          <Text style={styles.mainHeading}>Chats</Text>
          <FlatList
          data={[
            {name: 'Devin', message: 'Hello'},
            {name: 'Jackson', message: 'Hello'},
            {name: 'James', message: 'Hello'},
            {name: 'Joel', message: 'Hello'},
            {name: 'John', message: 'Hello'},
            {name: 'Jillian', message: 'Hello'},
            {name: 'Jimmy', message: 'Hello'},
            {name: 'Julie', message: 'Hello'},
            {name: 'Julie', message: 'Hello'},
            {name: 'Julie', message: 'Hello'},
            {name: 'Julie', message: 'Hello'},
            {name: 'Julie', message: 'Hello'},
            {name: 'Julie', message: 'Hello'},
            {name: 'Julie', message: 'Hello'},
            {name: 'Julie', message: 'Hello'},
            {name: 'Julie', message: 'Hello'},
            {name: 'Julie', message: 'Hello'},
            {name: 'Julie', message: 'Hello'},
            {name: 'Juli', message: 'Hello1'},
          ]}
          renderItem={({item}) =>
          <View style={styles.itemContainer}>
            <Image source={require('../../images/main.jpg')} style={styles.image} />
            <View style={{flex: 1, flexDirection: 'column', paddingLeft: 20, paddingRight: 10,justifyContent: 'center',}}>
              <Text style={styles.itemTitleText}>{item.name}</Text>
              <Text style={styles.itemText}>{item.message}</Text>
            </View>
          </View>
          }
          >
          </FlatList>
        </ImageBackground>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  itemText: {
    color: '#ffffff',
    fontSize: 10,
  },
  itemTitleText: {
    fontSize: 20,
    color: '#ffffff',
  },
  contentContainer: {
    width: '100%',
    height: '100%'
  },
  image: {
    width: 50,
    height: 50,
    borderRadius: 40,
    borderWidth: 1,
    borderColor: '#cb1b7b',
  },
  itemContainer: {
    flexDirection: 'row',
    flex: 1,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10,
    paddingBottom: 10,
    borderBottomColor: '#ffffff',
    borderBottomWidth: 1,
  },
  mainHeading: {
    fontWeight: "bold",
    fontSize: 20,
    textAlign: 'center',
    color: "#ffffff",
    marginBottom: 10,
    marginTop: 15,
  },
});
