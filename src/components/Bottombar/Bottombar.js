import React, {Component} from 'react';
import {StyleSheet, View, Image, ImageBackground, Button,} from 'react-native';

import Toast from 'react-native-simple-toast';
import { BottomNavigation, Text, Drawer } from 'react-native-paper';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {DrawerNavigator, NavigationActions, DrawerActions} from 'react-navigation';
import Dashboard from '../Dashboard/Dashboard';
import Chat from './Chat';
import Home from './Home';
import Search from './Search';
import Explore from './Explore';
//import Icon from 'react-native-vector-icons/FontAwesome';


export default class Bottombar extends Component {

  constructor(props) {
    super(props);
    Toast.show("Click on the last icon for Top Navigation bar example!");
    
  }
    state = {
        index: 0,
        routes: [
          { key: 'home', title: 'Home', icon: 'home' , color: '#114d91'},
          { key: 'explore', title: 'Explore', icon: 'explore',color: '#114d91' },
          { key: 'chat', title: 'Chat', icon: 'message', color: '#114d91'},
          { key: 'search', title: 'Search', icon: 'search', color: '#114d91' },
          { key: 'menu', title: 'Menu', icon: 'menu' , color: '#114d91'},
        ],
      };
    
      _handleIndexChange = index => this.setState({ index });
    
      /*_handleTabPress = (index) => {
        const { navigationState, onTabPress, onIndexChange } = this.props;
    
        if (onTabPress) {
          onTabPress({
            route: navigationState.routes[index],
          });
        }
    
        if (index !== navigationState.index) {
          onIndexChange(index);
        }
      };*/

      _renderScene = BottomNavigation.SceneMap({
        home: Dashboard,
        explore: Explore,
        chat: Chat,
        search: Search,
        menu: Search,

      });
    
      render() {
        return (
          <BottomNavigation
            navigationState={this.state}
            onIndexChange={this._handleIndexChange}
            renderScene={this._renderScene}
            onTabPress={(route) => {
              if(route.route.title == 'Menu'){
                this.props.navigation.dispatch(DrawerActions.openDrawer())
              }
              this.state.index = route.route.index;
            }}
            onIndexChange={(index) => {
              if(index!=4){
                this.setState({ index })
              }
            }}
          />
        );
      }
}