import React, {Component} from 'react';
import {StyleSheet, Text, View, Button, Image, ImageBackground} from 'react-native';

export default class Welcome extends Component {
  render() {
    const { navigate } = this.props.navigation;
    return (
      <ImageBackground source={require('./bg.png')} style={{width: '100%', height: '100%'}} >
      <View style={styles.container}>
        <View style={styles.loginContainer}>
        <Image style={styles.logo}
        resizeMode="contain"
        source={require('./logo.png')}
        />
        </View>
        <View style={styles.formContainer}>
          <View style={styles.buttonContainer}>
            <Button style={styles.input}
              title="Login with Email"
              color="#cb1b7b"
              accessibilityLabel="Learn more about this purple button"
              onPress={() =>
                navigate('Login')
              }
              />
          </View>
        <View style={styles.buttonContainer}>
          <Button style={styles.input}
            title="Login with Facebook"
            color="#4267b2"
            accessibilityLabel="Learn more about this purple button"
            />
        </View>
        <View style={styles.buttonContainer}>
            <Button style={styles.input}
              title="SignUp"
              onPress={() =>
                navigate('Register', { name: 'Jane' })
              }
              color="#808080"
              accessibilityLabel="Learn more about this purple button"
              />
        </View>
      </View>
    </View>
  </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    formContainer: {
      padding: 20,
    },
    buttonContainer: {
      marginBottom: 10,
    },
    input:{
        height: 40,
    },
    loginContainer:{
        alignItems: 'center',
        height: 200,
        justifyContent: 'center'
    },
    logo: {
        position: 'absolute',
        width: 300,
        height: 100
    }
  });
