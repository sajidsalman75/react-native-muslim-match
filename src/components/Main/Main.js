import React, {Component} from 'react';
import {StyleSheet, Text, View, TouchableOpacity, Image, ImageBackground, TextInput} from 'react-native';


export default class Welcome extends Component {
    static navigationOptions = {
        headerTransparent: 'true',
      };
    render() {
    const { navigate } = this.props.navigation;
    return (
    <ImageBackground source={require('../../images/bg.png')} style={{width: '100%', height: '100%'}} >
        <View style={styles.container}>
            <View style={styles.loginContainer}>
                <Image style={styles.logo}
                resizeMode="contain"
                source={require('../../images/logo.png')}
                />
            </View>
            <View style={styles.inputContainer}>
                <TextInput style = {styles.input}
                    autoCapitalize="none"
                    onSubmitEditing={() => this.passwordInput.focus()}
                    autoCorrect={false}
                    returnKeyType="next"
                    keyboardType= "email-address"
                    placeholder='Enter Email Address'
                    placeholderTextColor='#ffffff' 
                    />
            </View>
            <TouchableOpacity
            style={styles.buttonContainer}
            activeOpacity = { .9 }
            onPress={() => {
                navigate('Verification',{
                    status: 'Verifvation code has been sent to the provided email address.',
                });
            }}
            >
                <Text style={styles.buttonText}> EMAIL </Text>
            </TouchableOpacity>
            <View style={{flexDirection: 'row'}}>
                <View style={{flex: 1,  height: 20, borderColor: "rgba(225,225,225,0.7)", borderBottomWidth: 1, }} />
                <View style={{flex: 0.2, flexDirection:"row", marginTop:10, justifyContent: "center"}}><Text style={{height: 20, color:"rgba(225,225,225,0.7)"}}>OR</Text></View>
                <View style={{flex: 1, height: 20, borderColor: "rgba(225,225,225,0.7)", borderBottomWidth: 1, }} />
            </View>
            <TouchableOpacity
            style={styles.fbButtonContainer}
            activeOpacity = { .9 }
            >
                <Text style={styles.buttonText}> FACEBOOK </Text>
            </TouchableOpacity>
            <View style={{flex: 1,flexDirection: 'column', color: "rgba(225,225,225,0.7)",  justifyContent:"flex-end"}}>
            <Text style={{color: "rgba(225,225,225,0.7)",fontSize: 12, textAlign: "center"}}>By continuing you agree to 
                our <Text style={{textDecorationLine: "underline"}}>Terms</Text> and <Text style={{textDecorationLine: "underline"}}>Privacy Policy.</Text>
            </Text>
            </View>
            
        </View>
    </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
    },
    buttonContainer: {
      marginBottom: 10,
    },
    loginContainer:{
        alignItems: 'center',
        height: 200,
        justifyContent: 'center'
    },
    logo: {
        position: 'absolute',
        width: 300,
        height: 100
    },
    buttonContainer:{
        borderRadius: 10,
        color: '#fff',
        textAlign: 'center',
        fontWeight: '700',
        borderWidth: 1,
        borderColor: '#cb1b7b',
        paddingVertical: 10,
        marginTop: 10,
        backgroundColor: "#cb1b7b",
      },
      fbButtonContainer:{
        borderRadius: 10,
        textAlign: 'center',
        fontWeight: '700',
        borderWidth: 1,
        borderColor: '#4267b2',
        paddingVertical: 10,
        marginTop: 10,
        backgroundColor: "#4267b2",
      },
      buttonText:{
        color: '#fff',
        textAlign: 'center',
        fontWeight: '700',
      },
      inputContainer: {
        marginBottom: 10,
        borderColor: "#ffffff", 
        borderBottomWidth: 1, 
        padding: 0, 
        margin: 0,
      },
      input:{
        color: '#fff',
        fontSize: 22,
      },
  });
